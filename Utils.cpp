#include "Utils.h"

#include <algorithm>
#include <cctype>
#include <locale>
#include <string>

#ifdef MACOS
#include <unistd.h>
#endif
#ifdef LINUX
#include <unistd.h>
#endif
#ifdef WINDOWS
#include <windows.h>
#endif

vector<string> strSplit(const string& str, char sep) {
    vector<string> arr;
    string t;
    for (char i : str) {
        if (i == sep) {
            arr.push_back(t);
            t = "";
        } else
            t += i;
    }
    arr.push_back(t);
    return arr;
}

vector<string> strSplitWithQuotes(const string &s, char sep, char quotesOpen, char quotesClose) {
    // TODO: Add support for different types of quotes with their individual counters
    int quotesCounter = 0;
    string t;
    vector<string> r;
    for (char ch : s) {
        if (ch == quotesOpen)
            quotesCounter++;
        if (ch == quotesClose)
            quotesCounter--;
        if (ch == sep && quotesCounter == 0) {
            trim(t);
            r.push_back(t);
            t = "";
        } else
            t += ch;
    }
    trim(t);
    r.push_back(t);
    return r;
}

std::string stringReplace(std::string subject, const std::string& search, const std::string& replace) {
    size_t pos = 0;
    while ((pos = subject.find(search, pos)) != std::string::npos) {
        subject.replace(pos, search.length(), replace);
        pos += replace.length();
    }
    return subject;
}

// trim from start (in place)
void ltrim(std::string &s) {
    s.erase(s.begin(), std::find_if(s.begin(), s.end(), [](int ch) {
        return !std::isspace(ch);
    }));
}

// trim from end (in place)
void rtrim(std::string &s) {
    s.erase(std::find_if(s.rbegin(), s.rend(), [](int ch) {
        return !std::isspace(ch);
    }).base(), s.end());
}

// trim from both ends (in place)
void trim(std::string &s) {
    ltrim(s);
    rtrim(s);
}

bool is_number(const std::string& s) {
    return !s.empty() && std::find_if(s.begin(), s.end(), [](char c) { return !std::isdigit(c); }) == s.end();
}

bool ends_with(std::string const & value, std::string const & ending) {
    if (ending.size() > value.size()) return false;
    return std::equal(ending.rbegin(), ending.rend(), value.rbegin());
}