#ifndef ATOMS_CONTEXT_H
#define ATOMS_CONTEXT_H

#include <mutex>
#include <thread>

#include "json.hpp"
#include "duktape.h"
#include "utility.hpp"

#include "AtomId.h"

using namespace std;
using namespace nlohmann;

class Context {
private:

    class Timer {
    public:
        int interval;
        int iterations;
        int timeLeft;
        string code;
    };

public:
    explicit Context(uuid newid);
    Context();
    ~Context();

    uuid id;

    duk_context* ctx;

    json execute(const string& code, vector<AtomId> atoms = {}, vector<json> args = {},
        SimpleWeb::CaseInsensitiveMultimap* newHeadersInput = nullptr,
        SimpleWeb::CaseInsensitiveMultimap* newHeadersOutput = nullptr);

    static Context* contextGet(uuid contextId);

    void headerSet(const string& key, const string& value);
    json headersGet();

    uuid timerNew(const string& code, int interval, int iterations);
    void timerDelete(uuid timerId);

    SimpleWeb::CaseInsensitiveMultimap* headersInput;
    SimpleWeb::CaseInsensitiveMultimap* headersOutput;
private:
    recursive_mutex mutex;

    thread* timersThread;
    unordered_map<uuid, Timer*> timers;
    bool timersThreadActive = true;
    bool timersThreadFinished = false;
    void timersThreadProcess();

    static unordered_map<uuid, Context*> contexts;
    static recursive_mutex contextsMutex;
};

#endif //ATOMS_CONTEXT_H
