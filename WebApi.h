#ifndef ATOMS_API_H
#define ATOMS_API_H

#define USE_STANDALONE_ASIO
#define ASIO_STANDALONE

#ifdef MINGW
#include "winsock2.h"
#endif

#include "server_http.hpp"
#include "json.hpp"
#include "uuid.h"

#include "AtomId.h"

using namespace std;
using namespace nlohmann;
using namespace uuids;

using HttpServer = SimpleWeb::Server<SimpleWeb::HTTP>;

class WebApi {
public:
    explicit WebApi(string proto, int port);
    ~WebApi();

    uuid id;
    string proto;
    int port;

    static WebApi* webapiGet(uuid webapiId);

private:
    HttpServer server;
    thread* serverThread;

    static unordered_map<uuid, WebApi*> webapis;
    static recursive_mutex webapisMutex;
};

#endif //ATOMS_API_H
