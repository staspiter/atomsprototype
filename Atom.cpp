#include "Atom.h"

#include "Utils.h"

unordered_map<AtomId, Atom*> Atom::atoms;
recursive_mutex Atom::atomsMutex;
bool Atom::shutdown = false;

Atom::Atom(const AtomId& specificId) {
    if (atoms.find(specificId) != atoms.end())
        throw AtomIdAlreadyExistsException();

    id = specificId;

    atomsMutex.lock();
    atoms.insert(pair<AtomId, Atom*>(id, this));
    atomsMutex.unlock();
}

Atom::Atom(string cluster) : Atom::Atom(AtomId::AtomIdGenerate(std::move(cluster))) {}

Atom::~Atom() {
    lock();

    linksDisconnectAll();

    atomsMutex.lock();
    atoms.erase(id);
    atomsMutex.unlock();

    unlock();
}

void Atom::lock() {
    mutex.lock();
}

void Atom::unlock() {
    mutex.unlock();
}

unsigned long long int Atom::atomsTotal() {
    return atoms.size();
}

Atom* Atom::atomGet(AtomId id) {
    if (!id.valid)
        return nullptr;
    atomsMutex.lock();
    auto p = atoms.find(id);
    if (p == atoms.end()) {
        atomsMutex.unlock();
        return nullptr;
    }
    atomsMutex.unlock();
    return p->second;
}

void Atom::patch(json patchObj, string cluster) {
    // Create or patch atom by single object
    if (patchObj.is_object()) {
        AtomId atomId = AtomId::extractIdFromJson(patchObj);
        Atom* a = atomGet(atomId);
        if (a == nullptr) {
            if (atomId.valid)
                a = new Atom(atomId);
            else
                a = new Atom(cluster);
        }
        a->patchInternal(patchObj);
    }

    else if (patchObj.is_array())
        for (auto& i : patchObj) {
            // Destroy atom by "-id"
            if (i.is_string()) {
                auto existingIdStr = i.get<string>();
                if (!existingIdStr.empty() && existingIdStr[0] == '-') {
                    auto existingId = AtomId(existingIdStr.substr(1, existingIdStr.size() - 1));
                    if (existingId.valid)
                        delete atomGet(existingId);
                }
            }

                // Create or patch atom by object
            else if (i.is_object()) {
                AtomId atomId = AtomId::extractIdFromJson(i);
                Atom* a = atomGet(atomId);
                if (a == nullptr) {
                    if (atomId.valid)
                        a = new Atom(atomId);
                    else
                        a = new Atom(cluster);
                }
                a->patchInternal(i);
            }
        }
}

void Atom::patchInternal(json patchObj) {
    for (auto& i : patchObj.items()) {

        // Link update
        if (sizeof(i.key()) > 0 && i.key()[0] == '$' && i.key().size() > 1) {
            string linkNameWithDollar = string(i.key());
            string linkName = linkNameWithDollar.substr(1, linkNameWithDollar.size() - 1);

            // Value is a string (probably with atom id)
            if (i.value().is_string()) {
                auto linkIdStr = i.value().get<string>();

                // Delete link with key=linkName and "-id"
                if (!linkIdStr.empty() && linkIdStr[0] == '-') {
                    auto linkId = AtomId(linkIdStr.substr(1, linkIdStr.size() - 1));
                    if (linkId.valid)
                        linkDelete(linkName, linkId);
                }

                // Add link to atom with id
                else {
                    auto linkId = AtomId(linkIdStr);
                    if (linkId.valid)
                        linkAdd(linkName, linkId);
                }
            }

            // Create or patch atom and ensure a link with it
            else if (i.value().is_object()) {
                auto atomJson = i.value();
                AtomId newId = AtomId::extractIdFromJson(atomJson);
                Atom* a = atomGet(newId);
                if (a == nullptr) {
                    if (newId.valid)
                        a = new Atom(newId);
                    else
                        a = new Atom(this->id.cluster);
                }
                a->patchInternal(atomJson);
                linkAdd(linkName, a->id);
            }

            // Array of linked atoms
            else if (i.value().is_array()) {
                auto newAtomJsonArray = i.value();
                for (auto& atomJson : newAtomJsonArray) {
                    // Value is a string (probably with atom id)
                    if (atomJson.is_string()) {
                        auto linkIdStr = atomJson.get<string>();
                        if (!linkIdStr.empty() && linkIdStr[0] == '-') {
                            auto linkId = AtomId(linkIdStr.substr(1, linkIdStr.size() - 1));
                            if (linkId.valid)
                                linkDelete(linkName, linkId);
                        } else {
                            auto linkId = AtomId(linkIdStr);
                            if (linkId.valid)
                                linkAdd(linkName, linkId);
                        }
                    }

                    // Create or patch atom and ensure a link with it
                    else if (atomJson.is_object()) {
                        AtomId newId = AtomId::extractIdFromJson(atomJson);
                        Atom* a = atomGet(newId);
                        if (a == nullptr) {
                            if (newId.valid)
                                a = new Atom(newId);
                            else
                                a = new Atom(this->id.cluster);
                        }
                        a->patchInternal(atomJson);
                        linkAdd(linkName, a->id);
                    }
                }
            }
        }

        // Field update
        else
            fieldSet(i.key(), i.value());
    }
}

void Atom::dfs(vector<AtomId> &visited, int depth) {
    visited.push_back(this->id);

    if (depth == 0)
        return;

    lock();

    vector<AtomId> linkedAtoms;
    for (auto const& [key, linkedId] : links) {
        // Skip aspects
        if (key[0] == '$')
            continue;

        if (find(visited.begin(), visited.end(), linkedId) == visited.end())
            linkedAtoms.push_back(linkedId);
    }

    unlock();

    for (auto& aid : linkedAtoms) {
        auto a = atomGet(aid);
        if (a != nullptr)
            a->dfs(visited, depth - 1);
    }
}

json Atom::dump(vector<AtomId> atoms, int depth) {
    vector<AtomId> visited;
    for (auto &aid : atoms) {
        Atom* a = Atom::atomGet(aid);
        if (a != nullptr)
            a->dfs(visited, depth);
    }

    json dumpJson = json::array();
    json linksJson = json::array();

    for (auto& aid : visited) {
        auto a = atomGet(aid);
        if (a == nullptr)
            continue;

        a->lock();

        json j;
        j["$"] = aid.toString();
        for (auto const& [key, value] : a->fields)
            j[key] = json::parse(value);
        dumpJson += j;

        json j2;
        j2["$"] = aid.toString();
        int c = 0;
        for (auto& [key, linkedId] : a->links) {
            if (j2.find("$" + key) == j2.end())
                j2["$" + key] = linkedId.toString();
            else {
                json* linksArr = &j2["$" + key];
                if (linksArr->is_string()) {
                    json newLinksArr = json::array();
                    newLinksArr.push_back(*linksArr);
                    newLinksArr.push_back(linkedId.toString());
                    j2["$" + key] = newLinksArr;
                }
                else if (linksArr->is_array())
                    linksArr->push_back(linkedId.toString());
            }
            c++;
        }

        a->unlock();

        if (c > 0)
            linksJson += j2;
    }

    for (auto& j : linksJson)
        dumpJson += j;

    return dumpJson;
}

string readSelector(string s, string &rest) {
    string r;
    rest = "";

    trim(s);

    switch (s[0]) {
        case '.':
        case '/':
        case '<': {
            r += s[0];
            int i = 1;
            bool regexp = false;
            int regexpBracketCounter = 0;
            for (; i < s.length(); ++i) {
                if (s[i] == '{' && (i == 0 || s[i-1] != '\\'))
                    regexpBracketCounter++;
                if (s[i] == '}' && (i == 0 || s[i-1] != '\\'))
                    regexpBracketCounter--;
                if (s[i] == '{' && (i == 0 || s[i-1] != '\\') && !regexp && (regexpBracketCounter == 1)) {
                    regexp = true;
                    r += s[i];
                } else if (s[i] == '}' && (i == 0 || s[i-1] != '\\') && regexp && (regexpBracketCounter == 0)) {
                    regexp = false;
                    r += s[i];
                } else if (!regexp && (s[i] == '.'|| s[i] == '/' || s[i] == '<' || s[i] == '[' || s[i] == '(' || s[i] == '$'))
                    break;
                else
                    r += s[i];
            }
            if (i <= s.length() - 1)
                rest = s.substr(i, r.length() - i - 1);
            break;
        }

        case '(':
        case '[': {
            char bracketOpen = '(';
            char bracketClose = ')';
            if (s[0] == '[') {
                bracketOpen = '[';
                bracketClose = ']';
            }
            int bracketCounter = 0;
            int i = 0;
            bool regexp = false;
            int regexpBracketCounter = 0;
            for (; i < s.length(); ++i) {
                if (s[i] == '{' && (i == 0 || s[i-1] != '\\'))
                    regexpBracketCounter++;
                if (s[i] == '}' && (i == 0 || s[i-1] != '\\'))
                    regexpBracketCounter--;
                if (s[i] == '{' && (i == 0 || s[i-1] != '\\') && !regexp && (regexpBracketCounter == 1)) {
                    regexp = true;
                    r += s[i];
                } else if (s[i] == '}' && (i == 0 || s[i-1] != '\\') && regexp && (regexpBracketCounter == 0)) {
                    regexp = false;
                    r += s[i];
                } else if (!regexp && s[i] == bracketOpen) {
                    bracketCounter++;
                    r += s[i];
                } else if (!regexp && s[i] == bracketClose) {
                    bracketCounter--;
                    r += s[i];
                    if (bracketCounter == 0)
                        break;
                } else
                    r += s[i];
            }
            if (i + 1 <= s.length() - 1)
                rest = s.substr(i + 1ull, r.length() - i - 2);
            break;
        }

        default: {
            r += s[0];
            int i = 1;
            for (; i < s.length(); ++i) {
                if (s[i] == '.' || s[i] == '/' || s[i] == '<' || s[i] == '[' || s[i] == '(' || s[i] == '$')
                    break;
                else
                    r += s[i];
            }
            if (i <= s.length() - 1)
                rest = s.substr(i, r.length() - i - 1);
            break;
        };
    }

    trim(r);
    trim(rest);
    return r;
}

vector<AtomId> Atom::query(vector<AtomId> atoms, string selector) {
    trim(selector);

    if (selector.empty())
        return atoms;

    auto r = vector<AtomId>();
    string rest;
    auto token = readSelector(selector, rest);

    switch (token[0]) {
        case '.':
        case '/':
        case '<': {
            bool external = token[0] == '<';
            auto link = token.substr(1, token.length() - 1);
            trim(link);
            auto newAtoms = vector<AtomId>();
            for (auto &aid : atoms) {
                auto a = Atom::atomGet(aid);
                if (a) {
                    auto linked = a->linksGet(link, external);
                    newAtoms.insert(newAtoms.end(), linked.begin(), linked.end());
                }
            }
            r = query(newAtoms, rest);
            break;
        }

        case '[': {
            auto which = token.substr(1, token.length() - 2);
            trim(which);

            smatch m;

            if (which.empty()) {
                r = query(atoms, rest);

            } else if (is_number(which)) {
                int whichNum = atoi(which.c_str());
                if (whichNum < 0 || whichNum >= atoms.size())
                    r = query({}, rest);
                else
                    r = query({atoms[whichNum]}, rest);

            } else if (which == "last()") {
                if (atoms.empty())
                    r = query({}, rest);
                else
                    r = query({atoms[atoms.size() - 1]}, rest);

            } else if ((regex_search(which, m, regex("^'(.*?)'='(.*?)'$")) || regex_search(which, m, regex("^(.*?)=(.*?)$")))
                       && m.size() == 3) {
                string key = m.str(1);
                string value = m.str(2);
                auto newAtoms = vector<AtomId>();
                for (auto &aid : atoms) {
                    auto a = Atom::atomGet(aid);
                    if (!a)
                        continue;
                    auto realValue = a->fieldGet(key);
                    if (value[0] == '{' && value[value.length() - 1] == '}') {
                        if (regex_search(realValue, m, regex(value.substr(1, value.length() - 2))))
                            newAtoms.push_back(aid);
                    } else if (realValue == value || (realValue[0] == '\"' && realValue[realValue.length() - 1] == '\"'
                                                      && realValue.length() == value.length() + 2
                                                      && realValue.substr(1, realValue.length() - 2) == value))
                        newAtoms.push_back(aid);
                }
                r = query(newAtoms, rest);

            } else {
                string key = which;
                if (key[0] == '\'' && key[key.length() - 1] == '\'')
                    key = key.substr(1, key.length() - 2);
                auto newAtoms = vector<AtomId>();
                for (auto &aid : atoms) {
                    auto a = Atom::atomGet(aid);
                    if (!a)
                        continue;
                    auto realValue = a->fieldGet(key);
                    if (a->fieldExists(key))
                        newAtoms.push_back(aid);
                }
                r = query(newAtoms, rest);
            }

            break;
        }

        case '(': {
            auto united = token.substr(1, token.length() - 2);
            trim(united);
            auto unitedSplitted = strSplitWithQuotes(united, '|', '(', ')');
            auto newAtomsMap = unordered_map<AtomId,bool>();
            for (auto &s : unitedSplitted) {
                trim(s);
                auto newAtoms2 = query(atoms, s);
                for (auto &aid : newAtoms2)
                    if (newAtomsMap.find(aid) == newAtomsMap.end())
                        newAtomsMap.insert(pair<AtomId,bool>(aid, true));
            }
            vector<AtomId> newAtomsVector;
            for (auto &it : newAtomsMap)
                newAtomsVector.push_back(it.first);
            r = query(newAtomsVector, rest);
            break;
        }

        case '$': {
            auto aid = AtomId(token.substr(1));
            auto a = Atom::atomGet(aid);
            if (a != nullptr)
                r = query({a->id}, rest);
            else
                r = query({}, rest);
            break;
        };

        default: {
            auto aid = AtomId(token);
            auto a = Atom::atomGet(aid);
            if (a != nullptr)
                r = query({a->id}, rest);
            else
                r = query({}, rest);
            break;
        };
    }

    return r;
}

void Atom::destroy(const vector<AtomId>& atomsIds) {
    for (auto &aid : atomsIds)
        delete Atom::atomGet(aid);
}

void Atom::fieldSet(string key, string value) {
    if (key == "$")
        return;
    lock();
    if (value == "null") {
        if (fields.find(key) != fields.end())
            fields.erase(key);
    } else {
        fields.insert_or_assign(key, value);
    }
    watchersAddPatch({{key, json::parse(value)}});
    unlock();
}

void Atom::fieldSet(string key, json value) {
    if (key == "$")
        return;
    lock();
    if (value == nullptr || value.is_null()) {
        if (fields.find(key) != fields.end())
            fields.erase(key);
    } else {
        fields.insert_or_assign(key, value.dump());
    }
    watchersAddPatch({{key, value}});
    unlock();
}

string Atom::fieldGet(const string& key) {
    lock();
    auto f = fields.find(key);
    if (f != fields.end()) {
        string v = f->second;
        unlock();
        return v;
    }
    auto linksCopy = links;
    unlock();

    // Check aspects for the field
    for (auto const& [link, aid] : linksCopy)
        if (link[0] == '$' && aid.valid) {
            auto a = Atom::atomGet(aid);
            if (a != nullptr) {
                a->lock();
                string v = a->fieldGet(key);
                if (!v.empty()) {
                    a->unlock();
                    return v;
                }
                a->unlock();
            }
        }

    return "";
}

bool Atom::fieldExists(const string& key) {
    lock();
    string v;
    auto f = fields.find(key);
    if (f != fields.end()) {
        unlock();
        return true;
    }
    auto linksCopy = links;
    unlock();

    // Check aspects for the field
    for (auto const& [link, aid] : linksCopy)
        if (link[0] == '$' && aid.valid) {
            auto a = Atom::atomGet(aid);
            if (a != nullptr) {
                a->lock();
                if (a->fieldExists(key)) {
                    a->unlock();
                    return true;
                }
                a->unlock();
            }
        }

    return false;
}

vector<pair<string, string>> Atom::fieldsAllGet() {
    vector<pair<string, string>> r;

    lock();
    for (auto const& [key, value] : fields)
        r.emplace_back(key, value);
    auto linksCopy = links;
    unlock();

    // Check aspects for fields
    for (auto const& [link, aid] : linksCopy)
        if (link[0] == '$' && aid.valid) {
            auto a = Atom::atomGet(aid);
            if (a != nullptr) {
                a->lock();
                for (auto const& [key, value] : a->fields)
                    r.emplace_back(key, value);
                a->unlock();
            }
        }

    return r;
}

void Atom::linkAdd(const string& key, AtomId linkedId) {
    typedef multimap<string, AtomId>::iterator iterator;

    lock();

    // Prevent key&id duplicates
    std::pair<iterator, iterator> iterpair = links.equal_range(key);
    for (auto it = iterpair.first; it != iterpair.second; ++it)
        if (it->second == linkedId) {
            unlock();
            return;
        }

    links.insert(pair<string, AtomId>(key, linkedId));

    watchersAddPatch({{"$" + key, linkedId.toString()}});

    unlock();

    // Add external link for linked atom
    auto linkedAtom = atomGet(linkedId);
    if (linkedAtom != nullptr) {
        linkedAtom->lock();
        linkedAtom->linksExternal.insert(pair<string, AtomId>(key, this->id));
        linkedAtom->unlock();
    }
}

void Atom::linkDelete(const string& key, AtomId linkedId) {
    typedef multimap<string, AtomId>::iterator iterator;

    // Delete link

    lock();

    std::pair<iterator, iterator> iterpair = links.equal_range(key);
    for (auto it = iterpair.first; it != iterpair.second; ++it)
        if (it->second == linkedId) {
            links.erase(it);
            break;
        }

    watchersAddPatch({{"$" + key, "-" + linkedId.toString()}});

    unlock();

    // Delete external link
    auto linkedAtom = atomGet(linkedId);
    if (linkedAtom != nullptr) {
        linkedAtom->lock();
        std::pair<iterator, iterator> iterpair1 = linkedAtom->linksExternal.equal_range(key);
        for (auto it = iterpair1.first; it != iterpair1.second; ++it)
            if (it->second == this->id) {
                linkedAtom->linksExternal.erase(it);
                break;
            }
        linkedAtom->unlock();
    }
}

vector<AtomId> Atom::linksGet(string key, bool external) {
    vector<AtomId> v;

    lock();

    auto* linksRef = external ? &linksExternal : &links;

    if (key == "*") {
        // All linked
        for (auto const& [link, aid] : *linksRef)
            v.push_back(aid);

    } else if (key[0] == '{' && key[key.length() - 1] == '}') {
        // RegEx
        auto linkRegExStr = key.substr(1, key.length() - 2);
        auto linkRegEx = regex(linkRegExStr);
        smatch m;
        for (auto const& [link, aid] : *linksRef)
            if (regex_search(link, m, linkRegEx))
                v.push_back(aid);

    } else {
        // Strict
        typedef multimap<string, AtomId>::iterator iterator;
        std::pair<iterator, iterator> iterpair = linksRef->equal_range(key);
        for (auto it = iterpair.first; it != iterpair.second; ++it)
            v.push_back(it->second);
    }

    unlock();

    return v;
}

unsigned long long int Atom::linksCount() {
    return links.size();
}

void Atom::linksDisconnectAll() {
    lock();

    // Get outcoming links
    vector<pair<Atom*, string>> linkedAtoms;
    for (auto const& [key, aid] : links) {
        auto a = atomGet(aid);
        if (a != nullptr)
            linkedAtoms.emplace_back(a, key);
    }

    // Get incoming links
    vector<pair<Atom*, string>> linkedAtoms1;
    for (auto const& [key, aid] : linksExternal) {
        auto a = atomGet(aid);
        if (a != nullptr)
            linkedAtoms1.emplace_back(a, key);
    }

    unlock();

    for (auto& a : linkedAtoms)
        linkDelete(a.second, a.first->id);
    for (auto& a : linkedAtoms1)
        a.first->linkDelete(a.second, id);
}

void Atom::watch(uuid watcherId) {
    lock();
    Watcher::watcherGet(watcherId);
    watchers.insert(watcherId);
    unlock();
}

void Atom::unwatch(uuid watcherId) {
    lock();
    watchers.erase(watcherId);
    unlock();
}

void Atom::watchersAddPatch(const json& patch) {
    lock();
    for (auto &wid : watchers) {
        auto w = Watcher::watcherGet(wid);
        if (w)
            w->store(id, patch);
    }
    unlock();
}