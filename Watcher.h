#ifndef ATOMS_WATCHER_H
#define ATOMS_WATCHER_H

#include <mutex>

#include "json.hpp"
#include "uuid.h"

#include "AtomId.h"

class Watcher {
public:
    explicit Watcher(uuid id);
    ~Watcher();

    uuid id;

    void store(AtomId& aid, json patch);
    json extract();

    static Watcher* watcherGet(uuid watcherId);

    void lock();
    void unlock();

private:
    static unordered_map<uuid, Watcher*> watchers;
    static recursive_mutex watchersMutex;

    recursive_mutex mutex;

    // Private access to these fields should be in a locked clause
    unordered_map<AtomId, json> storedPatch;
};

#endif //ATOMS_WATCHER_H
