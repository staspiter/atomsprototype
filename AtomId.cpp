#include "AtomId.h"

#include "Utils.h"
#include "Atom.h"

uuid AtomId::clusterRootUuid = *uuid::from_string("00000000-0000-0000-0000-000000000000");

AtomId::AtomId() {
    valid = false;
};

AtomId AtomId::AtomIdGenerate(string newCluster) {
    AtomId a;
    a.cluster = std::move(newCluster);
    a.id = uuid_system_generator{}();
    a.valid = true;
    return a;
}

AtomId AtomId::AtomIdEmpty() {
    return AtomId();
}

AtomId::AtomId(string atomIdStr) {
    trim(atomIdStr);

    valid = false;

    int hyphensCount = 0, atSignCount = 0, atPos = -1;
    for (int i = 0; i < atomIdStr.length(); i++) {
        const char& c = atomIdStr[i];
        if (c == '-')
            hyphensCount++;
        if (c == '@') {
            atSignCount++;
            atPos = i;
        }
    }

    // Classical atom id
    if (hyphensCount == 4 && atSignCount == 1 && atPos == 36 && atomIdStr.length() > 37) {
        cluster = atomIdStr.substr(37);
        auto idOpt = uuid::from_string(atomIdStr.substr(0, 36));
        if (idOpt) {
            id = *idOpt;
            valid = true;
        }
        return;
    }

    // Cluster root atom id
    if (atomIdStr.length() > 0) {
        cluster = atomIdStr;
        id = clusterRootUuid;
        valid = true;
    }
}

string AtomId::toString() {
    if (!valid)
        return "";
    string uuidStr = uuids::to_string(id);
    if (uuidStr.compare("00000000-0000-0000-0000-000000000000") == 0)
        return cluster;
    else
        return uuidStr + "@" + cluster;
}

bool AtomId::isClusterRoot() {
    return id == clusterRootUuid;
}

bool AtomId::operator==(const AtomId &rhs) const {
    return cluster == rhs.cluster &&
           id == rhs.id &&
           valid == rhs.valid;
}

bool AtomId::operator!=(const AtomId &rhs) const {
    return !(rhs == *this);
}

vector<AtomId> AtomId::jsonToAtomIdsVector(json j) {
    if (j.type() != json::value_t::array)
        return {};

    vector<AtomId> result;
    for (int i = 0; i < j.size(); ++i)
        if (j[i].type() == json::value_t::string) {
            auto aid = AtomId(j[i].get<string>());
            if (aid.valid)
                result.push_back(aid);
        }
    return result;
}

AtomId AtomId::extractIdFromJson(json j) {
    if (j.find("$") != j.end()) {
        auto newAtomIdOpt = AtomId(j["$"].get<string>());
        if (newAtomIdOpt.valid)
            return newAtomIdOpt;
    }
    return AtomId::AtomIdEmpty();
}