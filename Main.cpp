#ifdef MINGW
#include "winsock2.h"
#endif

#include <iostream>
#include <fstream>
#include <cstdlib>

#include "Atom.h"
#include "Context.h"
#include "Utils.h"

using namespace std;

string GetLineFromCin() {
    string line;
    getline(cin, line);
    return line;
}

int main(int argc, char *argv[]) {
    Context initialCtx;

    bool console = true;

    for (int i = 1; i < argc; ++i) {
        string c = argv[i];
        if (c.empty())
            continue;

        // Parameters
        if (c[0] == '-') {
            if (c == "-noconsole") {
                console = false;
                cout << "Noconsole mode running" << endl;
            }
            continue;
        }

        // JS file inclusions
        if (ends_with(c, ".js")) {
            ifstream t(c);
            string str((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());
            initialCtx.execute(str);
            continue;
        }

        // Execute application argument as command
        initialCtx.execute(c);
    }

    if (!Atom::shutdown) {
        if (console) {
            // Console mode
            auto future = async(launch::async, GetLineFromCin);
            while (!Atom::shutdown) {
                if (future.wait_for(chrono::seconds(0)) == future_status::ready) {
                    auto str = future.get();
                    if (!str.empty())
                        initialCtx.execute(str);
                    if (Atom::shutdown)
                        break;
                    future = async(launch::async, GetLineFromCin);
                }
                this_thread::sleep_for(chrono::seconds(1));
            }
        } else
            while (!Atom::shutdown)
                this_thread::sleep_for(chrono::seconds(1));
    }

    exit(0);
}