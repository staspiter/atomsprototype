#include "gtest/gtest.h"

#include <fstream>
#include <string>
#include <iostream>
#include <regex>

#include "Atom.h"
#include "Utils.h"
#include "Context.h"

using namespace std;

TEST(AtomId, ParseAndStringify) {
    {
        AtomId i("429c0227-92b6-451e-9da9-7bbf7acc6191@server1");
        EXPECT_EQ(true, i.valid);
        EXPECT_EQ("server1", i.cluster);
        EXPECT_EQ("429c0227-92b6-451e-9da9-7bbf7acc6191", uuids::to_string(i.id));
        string s = i.toString();
        EXPECT_EQ("429c0227-92b6-451e-9da9-7bbf7acc6191@server1", s);
    }
    {
        AtomId i("server1");
        EXPECT_EQ(true, i.valid);
        EXPECT_EQ("server1", i.cluster);
        EXPECT_EQ("00000000-0000-0000-0000-000000000000", uuids::to_string(i.id));
        string s = i.toString();
        EXPECT_EQ("server1", s);
    }
    {
        AtomId i("00000000-0000-0000-0000-000000000000@server1");
        EXPECT_EQ(true, i.valid);
        EXPECT_EQ("server1", i.cluster);
        EXPECT_EQ("00000000-0000-0000-0000-000000000000", uuids::to_string(i.id));
        string s = i.toString();
        EXPECT_EQ("server1", s);
    }
    {
        AtomId i("");
        EXPECT_EQ(false, i.valid);
    }
}

TEST(Patch, TotalCount) {
    new Atom(AtomId("atoms_test"));

    Atom a("atoms_test");
    a.patchInternal(R"(
         {
              "$first_link": [
                   {
                        "foo": "bar"
                   },
                   {
                        "foo": "bar"
                   },
                   {
                        "foo": "bar"
                   }
              ]
         }
    )"_json);
    EXPECT_EQ(5, a.atomsTotal());
}

TEST(Patch, DifferentTypesOfFields) {
    Atom a("atoms_test");
    a.patchInternal(R"(
         {
              "string": "bar",
              "number": 10,
              "float": -0.56,
              "array": [1,2,"three"],
              "object": {"foo":"bar","key":"value"}
         }
    )"_json);

    EXPECT_EQ("\"bar\"", a.fieldGet("string"));
    EXPECT_EQ("10", a.fieldGet("number"));
    EXPECT_EQ("-0.56", a.fieldGet("float"));
    EXPECT_EQ("[1,2,\"three\"]", a.fieldGet("array"));
    EXPECT_EQ("{\"foo\":\"bar\",\"key\":\"value\"}", a.fieldGet("object"));
}

TEST(Links, PatchLinks) {
    Atom a("atoms_test");
    a.patchInternal(R"(
         {
              "$first_link": [
                   {
                        "$": "429c0227-92b6-451e-9da9-7bbf7acc6191@atoms_test",
                        "foo": "bar"
                   },
                   {
                        "foo": "bar"
                   }
              ]
         }
    )"_json);
    EXPECT_EQ(2, a.linksCount());

    a.patchInternal(R"(
         {
              "$first_link": [
                   "-429c0227-92b6-451e-9da9-7bbf7acc6191@atoms_test"
              ]
         }
    )"_json);
    auto v = a.linksGet("first_link");
    EXPECT_EQ(1, v.size());

    a.patchInternal(R"(
         {
              "$first_link": [
                   {
                        "foo": "bar"
                   }
              ]
         }
    )"_json);
    auto v1 = a.linksGet("first_link");
    EXPECT_EQ(2, v1.size());
}

TEST(Patch, PatchStatic) {
    Atom::patch(R"(
         {
              "$": "00000000-92b6-451e-9da9-7bbf7acc6191@atoms_test",
              "$link": [
                   {
                        "$": "11111111-3333-4444-5555-7bbf7acc6191@atoms_test",
                        "foo": "bar"
                   },
                   {
                        "$": "22222222-3333-4444-5555-7bbf7acc6191@atoms_test",
                        "foo": "bar"
                   }
              ]
         }
    )"_json, "atoms_test");

    auto v = Atom::atomGet(AtomId("00000000-92b6-451e-9da9-7bbf7acc6191@atoms_test"))->linksGet("link");
    EXPECT_EQ(2, v.size());

    Atom::patch(R"(
         [
              "-11111111-3333-4444-5555-7bbf7acc6191@atoms_test"
         ]
    )"_json, "atoms_test");
    v = Atom::atomGet(AtomId("00000000-92b6-451e-9da9-7bbf7acc6191@atoms_test"))->linksGet("link");
    EXPECT_EQ(1, v.size());

    Atom::patch(R"(
         [
              "-22222222-3333-4444-5555-7bbf7acc6191@atoms_test"
         ]
    )"_json, "atoms_test");
    v = Atom::atomGet(AtomId("00000000-92b6-451e-9da9-7bbf7acc6191@atoms_test"))->linksGet("link");
    EXPECT_EQ(0, v.size());

    // Add link to unknown atom
    Atom::patch(R"(
         [
              {
                   "$": "33333333-3333-4444-5555-7bbf7acc6191@atoms_test",
                   "foo": "bar"
              },
              {
                   "$": "33333333-3333-4444-5555-7bbf7acc6191@atoms_test",
                   "$linked": "10101010-92b6-451e-9da9-7bbf7acc6191@atoms_test"
              }
         ]
    )"_json, "atoms_test");
    v = Atom::atomGet(AtomId("33333333-3333-4444-5555-7bbf7acc6191@atoms_test"))->linksGet("linked");
    EXPECT_EQ(1, v.size());
}

TEST(Dump, DFSAndDump) {
    Atom::patch(R"(
         {
              "$": "atoms_test",
              "number": 10.4,
              "number2": 5,
              "array": ["string", 10.5],
              "stringField": "hello world",
              "$link": [
                   {
                        "$": "00000000-0000-0000-0000-000000000001@atoms_test",
                        "foo": "bar",
                        "$cycle": "00000000-0000-0000-0000-000000000000@atoms_test",
                        "$link": [
                             {
                                  "$": "00000000-0000-0000-0000-000000000003@atoms_test"
                             },
                             {
                                  "$": "00000000-0000-0000-0000-000000000004@atoms_test",
                                  "$cycle": "00000000-0000-0000-0000-000000000000@atoms_test"
                             }
                        ]
                   },
                   {
                        "$": "00000000-0000-0000-0000-000000000002@atoms_test",
                        "foo": "bar"
                   }
              ]
         }
    )"_json, "atoms_test");

    vector<AtomId> v;

    Atom::atomGet(AtomId("00000000-0000-0000-0000-000000000000@atoms_test"))->dfs(v, 0);
    EXPECT_EQ(1, v.size());
    v.clear();

    Atom::atomGet(AtomId("00000000-0000-0000-0000-000000000000@atoms_test"))->dfs(v, 1);
    EXPECT_EQ(3, v.size());
    v.clear();

    Atom::atomGet(AtomId("00000000-0000-0000-0000-000000000000@atoms_test"))->dfs(v);
    EXPECT_EQ(5, v.size());
    v.clear();

    // Make atoms dump

    string dumpCheck = R"([{"$":"atoms_test","array":["string",10.5],"number":10.4,"number2":5,"stringField":"hello world"},{"$":"00000000-0000-0000-0000-000000000001@atoms_test","foo":"bar"},{"$":"00000000-0000-0000-0000-000000000003@atoms_test"},{"$":"00000000-0000-0000-0000-000000000004@atoms_test"},{"$":"00000000-0000-0000-0000-000000000002@atoms_test","foo":"bar"},{"$":"atoms_test","$link":["00000000-0000-0000-0000-000000000001@atoms_test","00000000-0000-0000-0000-000000000002@atoms_test"]},{"$":"00000000-0000-0000-0000-000000000001@atoms_test","$cycle":"atoms_test","$link":["00000000-0000-0000-0000-000000000003@atoms_test","00000000-0000-0000-0000-000000000004@atoms_test"]},{"$":"00000000-0000-0000-0000-000000000004@atoms_test","$cycle":"atoms_test"}])";
    string dump = Atom::dump({AtomId("atoms_test")}).dump();
    EXPECT_EQ(dump, dumpCheck);

    // Destroy atoms

//    delete Atom::atomGet(AtomId("00000000-0000-0000-0000-000000000000@atoms_test"));
    delete Atom::atomGet(AtomId("00000000-0000-0000-0000-000000000001@atoms_test"));
    delete Atom::atomGet(AtomId("00000000-0000-0000-0000-000000000002@atoms_test"));
    delete Atom::atomGet(AtomId("00000000-0000-0000-0000-000000000003@atoms_test"));
    delete Atom::atomGet(AtomId("00000000-0000-0000-0000-000000000004@atoms_test"));

    EXPECT_EQ(nullptr, Atom::atomGet(AtomId("00000000-0000-0000-0000-000000000001@atoms_test")));

    // Create atoms again from the dump

    Atom::patch(json::parse(dump), "atoms_test");

    // Check atoms count in the graph

    Atom::atomGet(AtomId("00000000-0000-0000-0000-000000000000@atoms_test"))->dfs(v);
    EXPECT_EQ(5, v.size());
    v.clear();

    // Compare lengths of check dump and generated dump

    dump = Atom::dump({AtomId("00000000-0000-0000-0000-000000000000@atoms_test")}).dump();
    EXPECT_EQ(dump.length(), dumpCheck.length());
}

TEST(Query, ReadSelector) {
    {
        string r;
        string s = readSelector(".first.second", r);
        EXPECT_EQ(".first", s);
        EXPECT_EQ(".second", r);
    }

    {
        string r;
        string s = readSelector("atoms_test1/$atoms_test", r);
        EXPECT_EQ("atoms_test1", s);
        EXPECT_EQ("/$atoms_test", r);
    }

    {
        string r;
        string s = readSelector("/$atoms_test", r);
        EXPECT_EQ("/", s);
        EXPECT_EQ("$atoms_test", r);
    }

    {
        string r;
        string s = readSelector(".first.second", r);
        EXPECT_EQ(".first", s);
        EXPECT_EQ(".second", r);
    }

    {
        string r;
        string s = readSelector(".{^(.*?)(foo|bar)$}.second", r);
        EXPECT_EQ(".{^(.*?)(foo|bar)$}", s);
        EXPECT_EQ(".second", r);
    }

    {
        string r;
        string s = readSelector("[field={^(.*?)[a-z][[[[(foo|bar)$}[]].second", r);
        EXPECT_EQ("[field={^(.*?)[a-z][[[[(foo|bar)$}[]]", s);
        EXPECT_EQ(".second", r);
    }

    {
        string r;
        string s = readSelector("(.link1{((}(()()).link2,[field=value]).second", r);
        EXPECT_EQ("(.link1{((}(()()).link2,[field=value])", s);
        EXPECT_EQ(".second", r);
    }

    {
        string r;
        string s = readSelector("..second", r);
        EXPECT_EQ(".", s);
        EXPECT_EQ(".second", r);
    }

    {
        string r;
        string s = readSelector("00000000-0000-0000-0000-000000000000@atoms_test (.link1{((}(()()).link2,[field=value]).second", r);
        EXPECT_EQ("00000000-0000-0000-0000-000000000000@atoms_test", s);
        EXPECT_EQ("(.link1{((}(()()).link2,[field=value]).second", r);
    }
}

TEST(Query, GlobalPath) {
    Atom::atomGet(AtomId("atoms_test"))->patchInternal(R"(
         {
              "$a": [
                   {
                        "$": "10000000-0000-0000-0000-000000000001@atoms_test",
                        "$anotherLink": [
                             {
                                  "$": "20000000-0000-0000-0000-000000000001@atoms_test"
                             },
                             {
                                  "$": "30000000-0000-0000-0000-000000000001@atoms_test"
                             }
                        ],
                        "$onemorelink": {}
                   },
                   {
                        "$": "40000000-0000-0000-0000-000000000001@atoms_test"
                   }
              ],
              "$a1": {"foo": "bar1", "k": 2},
              "$a2": {"foo": "bar", "k": 3}
         }
    )"_json);

    {
        EXPECT_EQ(1, Atom::query({}, "atoms_test").size());
    }

    {
        EXPECT_EQ(2, Atom::query({}, "atoms_test/a/anotherLink").size());
    }

    {
        EXPECT_EQ(1, Atom::query({}, "atoms_test/a2").size());
    }
}

TEST(Query, LinkedAtoms) {
    Atom a("atoms_test");
    a.patchInternal(R"(
         {
              "number": 10.4,
              "number2": 5,
              "array": ["string", 10.5],
              "stringField": "hello world",
              "$link": [
                   {
                        "$": "10000000-0000-0000-0000-000000000000@atoms_test",
                        "foo": "bar",
                        "k": 0,
                        "$anotherLink": [
                             {
                                  "$": "20000000-0000-0000-0000-000000000000@atoms_test",
                                  "foo": "bar"
                             },
                             {
                                  "$": "30000000-0000-0000-0000-000000000000@atoms_test",
                                  "foo": "bar"
                             }
                        ],
                        "$onemorelink": {}
                   },
                   {
                        "$": "40000000-0000-0000-0000-000000000000@atoms_test",
                        "foo": "bar",
                        "k": 1
                   }
              ],
              "$link1": {"foo": "bar1", "k": 2},
              "$link2": {"foo": "bar", "k": 3}
         }
    )"_json);

    EXPECT_EQ(Atom::query({}, "atoms_test1/$atoms_test").size(), 1);

    EXPECT_EQ(AtomId("10000000-0000-0000-0000-000000000000@atoms_test"), Atom::query({}, "10000000-0000-0000-0000-000000000000@atoms_test")[0]);

    EXPECT_EQ(Atom::query({}, "10000000-0000-0000-0000-000000000000@atoms_test.asdsada.adasda$atoms_test").size(), 1);
    EXPECT_EQ(Atom::query({}, "10000000-0000-0000-0000-000000000000@atoms_test/$atoms_test").size(), 1);

    {
        auto q = Atom::query({a.id}, ".link");
        EXPECT_EQ(2, q.size());
        EXPECT_EQ("10000000-0000-0000-0000-000000000000@atoms_test", q[0].toString());
        EXPECT_EQ("40000000-0000-0000-0000-000000000000@atoms_test", q[1].toString());
    }

    {
        auto q = Atom::query({a.id}, ".link.anotherLink");
        EXPECT_EQ(2, q.size());
        EXPECT_EQ("20000000-0000-0000-0000-000000000000@atoms_test", q[0].toString());
        EXPECT_EQ("30000000-0000-0000-0000-000000000000@atoms_test", q[1].toString());
    }

    {
        auto q = Atom::query({}, a.id.toString() + "/link/anotherLink");
        EXPECT_EQ(2, q.size());
        EXPECT_EQ("20000000-0000-0000-0000-000000000000@atoms_test", q[0].toString());
        EXPECT_EQ("30000000-0000-0000-0000-000000000000@atoms_test", q[1].toString());
    }

    {
        EXPECT_EQ(4, Atom::query({a.id}, ".{link\\d?}").size());
        EXPECT_EQ(2, Atom::query({a.id}, ".{link\\d+}").size());
        EXPECT_EQ(0, Atom::query({a.id}, ".{link\\d+}.anotherLink").size());
        EXPECT_EQ(2, Atom::query({a.id}, ".{link\\d?}.anotherLink").size());
        EXPECT_EQ(3, Atom::query({a.id}, ".link.*").size());
    }

    {
        auto q = Atom::query({}, "20000000-0000-0000-0000-000000000000@atoms_test < anotherLink < link");
        EXPECT_EQ(1, q.size());
        if (q.size() > 0)
            EXPECT_EQ("\"hello world\"", Atom::atomGet(q[0])->fieldGet("stringField"));
    }

    {
        EXPECT_EQ(4, Atom::query({a.id}, ".*['foo']").size());
        EXPECT_EQ(4, Atom::query({a.id}, ".*[foo]").size());
        EXPECT_EQ(3, Atom::query({a.id}, ".*['foo'='bar']").size());
        EXPECT_EQ(3, Atom::query({a.id}, ".*[foo][foo=bar]").size());
        EXPECT_EQ("0", Atom::atomGet(Atom::query({a.id}, ".*[foo][0]")[0])->fieldGet("k"));
        EXPECT_EQ("1", Atom::atomGet(Atom::query({a.id}, ".*[foo][1]")[0])->fieldGet("k"));
        EXPECT_EQ("3", Atom::atomGet(Atom::query({a.id}, ".*[foo][last()]")[0])->fieldGet("k"));
    }

    {
        EXPECT_EQ(2, Atom::query({a.id}, ".*([foo=bar][k=0]|[foo=bar1])").size());
    }
}

TEST(Utils, StringFunctions) {
    auto v = strSplitWithQuotes("fwefwefwe, (1,,23())123, , 12312, (()(())) ", ',', '(', ')');

    EXPECT_EQ(5, v.size());
    EXPECT_EQ("fwefwefwe", v[0]);
    EXPECT_EQ("(1,,23())123", v[1]);
    EXPECT_EQ("", v[2]);
    EXPECT_EQ("12312", v[3]);
    EXPECT_EQ("(()(()))", v[4]);
}

TEST(Watcher, WatcherBasics) {
    uuid watcher = uuid_system_generator{}();

    Atom a(AtomId("00000000-1000-0000-0000-000000000000@atoms_test"));

    a.watch(watcher);

    for (int i = 0; i < 5; ++i)
        a.patchInternal(R"(
            {
                "foo": "bar",
                "bar": "foo"
            }
        )"_json);
    EXPECT_EQ("[{\"$\":\"00000000-1000-0000-0000-000000000000@atoms_test\",\"bar\":\"foo\",\"foo\":\"bar\"}]", Watcher::watcherGet(watcher)->extract().dump());

    a.unwatch(watcher);

    a.patchInternal(R"(
            {
                "foo": "bar123"
            }
        )"_json);

    EXPECT_EQ("[]", Watcher::watcherGet(watcher)->extract().dump());

    a.watch(watcher);

    a.patchInternal(R"(
        {
            "$node": {
                "$": "00000000-2000-0000-0000-000000000000@atoms_test",
                "text": "Lorem ipsum"
            }
        }
    )"_json);
    EXPECT_EQ("[{\"$\":\"00000000-1000-0000-0000-000000000000@atoms_test\",\"$node\":\"00000000-2000-0000-0000-000000000000@atoms_test\"}]", Watcher::watcherGet(watcher)->extract().dump());

    a.patchInternal(R"(
        {
            "$node": "-00000000-2000-0000-0000-000000000000@atoms_test"
        }
    )"_json);
    EXPECT_EQ("[{\"$\":\"00000000-1000-0000-0000-000000000000@atoms_test\",\"$node\":\"-00000000-2000-0000-0000-000000000000@atoms_test\"}]", Watcher::watcherGet(watcher)->extract().dump());

    Atom::atomGet(AtomId("00000000-2000-0000-0000-000000000000@atoms_test"))->watch(watcher);
    Atom::patch(R"(
        [
            {
                "$": "00000000-2000-0000-0000-000000000000@atoms_test",
                "$node2": {
                    "$": "00000000-3000-0000-0000-000000000000@atoms_test"
                },
                "text": "newtext"
            },
            {
                "$": "00000000-1000-0000-0000-000000000000@atoms_test",
                "foo": "bar"
            }
        ]
    )"_json, "atoms_test");
    EXPECT_EQ("[{\"$\":\"00000000-1000-0000-0000-000000000000@atoms_test\",\"foo\":\"bar\"},{\"$\":\"00000000-2000-0000-0000-000000000000@atoms_test\",\"$node2\":\"00000000-3000-0000-0000-000000000000@atoms_test\",\"text\":\"newtext\"}]", Watcher::watcherGet(watcher)->extract().dump());

    EXPECT_EQ("[]", Watcher::watcherGet(watcher)->extract().dump());
}

TEST(API, PatchAndDump) {
    auto ctx = Context();

    ctx.execute(R"(patch({
    $: "API_PatchAndDump",
    $children: [
        {
            $: "00000000-0000-0000-0000-000000000003@API_PatchAndDump",
            foo: "bar"
        },
        {
            $: "00000000-0000-0000-0000-000000000001@API_PatchAndDump",
            foo: "bar"
        },
        {
            $: "00000000-0000-0000-0000-000000000002@API_PatchAndDump",
            foo: "bar"
        }
    ]
    });)");

    EXPECT_EQ(ctx.execute("query('API_PatchAndDump').dump()").dump(),
              "[{\"$\":\"API_PatchAndDump\"},{\"$\":\"00000000-0000-0000-0000-000000000003@API_PatchAndDump\",\"foo\":\"bar\"},{\"$\":\"00000000-0000-0000-0000-000000000001@API_PatchAndDump\",\"foo\":\"bar\"},{\"$\":\"00000000-0000-0000-0000-000000000002@API_PatchAndDump\",\"foo\":\"bar\"},{\"$\":\"API_PatchAndDump\",\"$children\":[\"00000000-0000-0000-0000-000000000003@API_PatchAndDump\",\"00000000-0000-0000-0000-000000000001@API_PatchAndDump\",\"00000000-0000-0000-0000-000000000002@API_PatchAndDump\"]}]");
}

TEST(API, PatchAndDestroy) {
    auto ctx = Context();

    ctx.execute(R"(patch({
    $: "API_PatchAndDestroy",
    $children: [
        {
            $: "00000000-0000-0000-0000-000000000003@API_PatchAndDestroy",
            foo: "bar"
        },
        {
            $: "00000000-0000-0000-0000-000000000001@API_PatchAndDestroy",
            foo: "bar"
        },
        {
            $: "00000000-0000-0000-0000-000000000002@API_PatchAndDestroy",
            foo: "bar"
        }
    ]
    });
    query("API_PatchAndDestroy.children").destroy();
    )");

    EXPECT_EQ(ctx.execute("query('API_PatchAndDestroy').dump()").dump(), "[{\"$\":\"API_PatchAndDestroy\"}]");
}

TEST(API, PatchLocalGetSet) {
    auto ctx = Context();

    ctx.execute(R"(patch({
    $: "API_PatchLocal",
    $children: {
        foo: "bar"
    }
    });
    )");

    EXPECT_EQ(ctx.execute("query('API_PatchLocal/children').get('foo')").dump(), "[\"bar\"]");
    EXPECT_EQ(ctx.execute("query('API_PatchLocal/children').set('foo',123).get('foo')").dump(), "[123]");
    EXPECT_EQ(ctx.execute("query('API_PatchLocal/children').set('foo',123.45).get('foo')").dump(), "[123.45]");
    EXPECT_EQ(ctx.execute("query('API_PatchLocal/children').patch({'foo': {field: 'val'}}).get('foo')[0]").dump(), "{\"field\":\"val\"}");
}

TEST(API, FunctionsCallAndAspect) {
    auto ctx = Context();

    ctx.execute(R"(patch({
    $: "MathAspect",
    mul: function(a, b) {
        return a * b;
    },
    sum: function(a, b) {
        return a + b;
    }
    });
    )");

    ctx.execute(R"(patch({
    $: "API_FunctionsCall",
    $children: [{
        foo: "bar",
        $$math: "MathAspect",
        formula: function(a, b) {
            return sum(sum(a, b)[0], mul(a, b)[0])[0];
        }
    },
    {
        foo: "bar",
        $$math: "MathAspect",
        formula: function(a, b) {
            return mul(sum(a, b)[0], mul(a, b)[0])[0];
        }
    }
    ]
    });
    )");

    EXPECT_EQ(ctx.execute("query('API_FunctionsCall/childrenS').atoms").dump(), "[]");
    EXPECT_EQ(ctx.execute("query('API_FunctionsCall/childrenS[0]').atoms").dump(), "[]");
    EXPECT_EQ(ctx.execute("query('API_FunctionsCall').query('.children[0]').formula(10, 5)[0]").dump(), "65");
    EXPECT_EQ(ctx.execute("query('API_FunctionsCall/children').formula(10, 5)").dump(), "[65,750]");
}

TEST(API, ContextQueryCall) {
    auto ctx = Context();

    ctx.execute(R"(patch({
        $: "ContextQueryCall",
        f: function() {
            return field;
        },
        field: "Hello"
    });
    var c = contextNew();
    )");

    EXPECT_EQ(ctx.execute(R"(
    query('ContextQueryCall').call({
        context: c,
        code: function() {
            return f()[0];
        }
    });
    )").dump(), "\"Hello\"");
}