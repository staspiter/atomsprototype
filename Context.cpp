#include "Context.h"

#include <iostream>
#include <fstream>
#include <streambuf>

#include "Atom.h"
#include "Utils.h"
#include "base64.h"

using namespace std;

unordered_map<uuid, Context*> Context::contexts;
recursive_mutex Context::contextsMutex;

string duk_function_to_bytecode(duk_context *ctx) {
    duk_dump_function(ctx);
    void* ptr;
    duk_size_t sz;
    ptr = duk_get_buffer(ctx, -1, &sz);
    return "bytecode_" + base64_encode((unsigned char *)ptr, sz);
}

json duk_json_encode_bytecode(duk_context *ctx, duk_idx_t idx) {
    json j;
    bool isArray = false;
    if (duk_is_array(ctx, idx)) {
        j = json::array();
        isArray = true;
    } else
        j = json::object();

    duk_enum(ctx, idx, DUK_ENUM_OWN_PROPERTIES_ONLY);
    while (duk_next(ctx, -1, 1)) {
        string key = duk_safe_to_string(ctx, -2);

        switch (duk_get_type(ctx, -1)) {
            case DUK_TYPE_STRING:
                if (isArray)
                    j.push_back(string(duk_get_string(ctx, -1)));
                else
                    j[key] = string(duk_get_string(ctx, -1));
                break;

            case DUK_TYPE_NUMBER: {
                double d = duk_get_number(ctx, -1), intpart;
                bool f = modf(d, &intpart) != 0.0;

                if (isArray) {
                    if (f)
                        j.push_back(d);
                    else
                        j.push_back((int)d);
                }
                else {
                    if (f)
                        j[key] = d;
                    else
                        j[key] = (int)d;
                }
                break;
            }

            case DUK_TYPE_BOOLEAN:
                if (isArray)
                    j.push_back((bool)duk_get_boolean(ctx, -1));
                else
                    j[key] = (bool)duk_get_boolean(ctx, -1);
                break;

            case DUK_TYPE_OBJECT:
                if (duk_is_function(ctx, -1)) {
                    if (!duk_is_c_function(ctx, -1)) {
                        string s = duk_function_to_bytecode(ctx);
                        if (isArray)
                            j.push_back(s);
                        else
                            j[key] = s;
                    } else {
                        if (isArray)
                            j.push_back("c_function");
                        else
                            j[key] = "c_function";
                    }
                }
                else if (duk_is_object(ctx, -1) || duk_is_array(ctx, -1)) {
                    if (isArray)
                        j.push_back(duk_json_encode_bytecode(ctx, -1));
                    else
                        j[key] = duk_json_encode_bytecode(ctx, -1);
                }
                break;

            default:
                if (isArray)
                    j.push_back(nullptr);
                else
                    j[key] = nullptr;
                break;
        }
        duk_pop_2(ctx);
    }
    duk_pop(ctx);

    return j;
}

json duk_value_to_json(duk_context *ctx, duk_idx_t idx) {
    if (duk_is_string(ctx, idx))
        return json(string(duk_get_string(ctx, idx)));

    if (duk_is_number(ctx, idx)) {
        double d = duk_get_number(ctx, idx), intpart;
        bool f = modf(d, &intpart) != 0.0;
        if (f)
            return json::parse(to_string(d));
        else
            return json::parse(to_string((int)d));
    }

    if (duk_is_boolean(ctx, idx))
        return json::parse(duk_get_boolean(ctx, idx) ? "true" : "false");

    if (duk_is_function(ctx, idx))
        return json::parse("\"" + duk_function_to_bytecode(ctx) + "\"");

    if (duk_is_object(ctx, idx))
        return duk_json_encode_bytecode(ctx, idx);

    json null_value;
    return null_value;
}

void duk_push_value_from_json(duk_context *ctx, json j) {
    if (j.type() == json::value_t::string && j.get<string>().find("bytecode_") == 0) {
        auto bytecode = j.get<string>();
        string decodedBytecode = base64_decode(bytecode.substr(9));
        duk_push_external_buffer(ctx);
        duk_config_buffer(ctx, -1, &decodedBytecode[0], decodedBytecode.size());
        duk_load_function(ctx);
    }
    else if (j.type() == json::value_t::string)
        duk_push_string(ctx, j.get<string>().c_str());

    else if (j.type() == json::value_t::number_float)
        duk_push_number(ctx, j.get<double>());

    else if (j.type() == json::value_t::number_integer)
        duk_push_number(ctx, j.get<int>());

    else if (j.type() == json::value_t::number_unsigned)
        duk_push_number(ctx, j.get<unsigned long int>());

    else if (j.type() == json::value_t::boolean)
        duk_push_boolean(ctx, j.get<bool>());

    else if (j.type() == json::value_t::object || j.type() == json::value_t::array) {
        duk_push_string(ctx, j.dump().c_str());
        duk_json_decode(ctx, -1);
    }

    else
        duk_push_null(ctx);
}

vector<AtomId> getAtomsFromThis(duk_context *ctx) {
    // Atoms
    duk_push_this(ctx);
    if (duk_get_type(ctx, -1) == DUK_TYPE_UNDEFINED) {
        duk_pop(ctx);
        duk_push_global_object(ctx);
    }
    duk_get_prop_string(ctx, -1, "atoms");
    int atomsLength = duk_get_length(ctx, -1);
    vector<AtomId> atoms;
    for (int i = 0; i < atomsLength; i++) {
        duk_get_prop_index(ctx, -1, i);
        auto aid = AtomId(duk_get_string(ctx, -1));
        duk_pop(ctx);
        if (aid.valid)
            atoms.push_back(aid);
    }
    duk_pop(ctx);

    return atoms;
}

bool hasQueryObject(duk_context *ctx) {
    bool r = false;
    duk_push_this(ctx);
    if (duk_is_object(ctx, -1) && duk_has_prop_string(ctx, -1, "atoms"))
        r = true;
    duk_pop(ctx);
    return r;
}

static duk_ret_t internalQuery(duk_context *ctx);
static duk_ret_t internalDump(duk_context *ctx);
static duk_ret_t internalDestroy(duk_context *ctx);
static duk_ret_t internalGet(duk_context *ctx);
static duk_ret_t internalSet(duk_context *ctx);
static duk_ret_t internalPatchLocal(duk_context *ctx);
static duk_ret_t internalWatch(duk_context *ctx);
static duk_ret_t internalUnwatch(duk_context *ctx);
static duk_ret_t internalCall(duk_context *ctx);
static duk_ret_t internalFunctionCall(duk_context *ctx);

void pushQueryObject(duk_context *ctx, vector<AtomId> atoms, bool globalObject) {
    duk_idx_t obj_idx = 0;
    if (!globalObject)
        obj_idx = duk_push_object(ctx);

    // Put atoms array
    duk_idx_t arr_idx = duk_push_array(ctx);
    int i = 0;
    for (auto &aid : atoms) {
        auto a = Atom::atomGet(aid);
        if (a != nullptr) {
            duk_push_string(ctx, aid.toString().c_str());
            duk_put_prop_index(ctx, arr_idx, i);
            i++;
            auto v = a->fieldsAllGet();
            for (auto const& [key, value] : v) {
                if (value.find("\"bytecode_") == 0) {
                    duk_idx_t func_idx = duk_push_c_function(ctx, internalFunctionCall, DUK_VARARGS);
                    duk_push_string(ctx, key.c_str());
                    duk_put_prop_string(ctx, func_idx, "function_name");
                } else
                    duk_push_value_from_json(ctx, json::parse(value));
                if (globalObject)
                    duk_put_global_string(ctx, key.c_str());
                else
                    duk_put_prop_string(ctx, obj_idx, key.c_str());
            }
        }
    }
    if (globalObject)
        duk_put_global_string(ctx, "atoms");
    else
        duk_put_prop_string(ctx, obj_idx, "atoms");

    // Put standard functions

    duk_push_c_function(ctx, internalQuery, 1);
    if (globalObject)
        duk_put_global_string(ctx, "query");
    else
        duk_put_prop_string(ctx, obj_idx, "query");

    duk_push_c_function(ctx, internalDump, DUK_VARARGS);
    if (globalObject)
        duk_put_global_string(ctx, "dump");
    else
        duk_put_prop_string(ctx, obj_idx, "dump");

    duk_push_c_function(ctx, internalDestroy, 0);
    if (globalObject)
        duk_put_global_string(ctx, "destroy");
    else
        duk_put_prop_string(ctx, obj_idx, "destroy");

    duk_push_c_function(ctx, internalGet, 1);
    if (globalObject)
        duk_put_global_string(ctx, "get");
    else
        duk_put_prop_string(ctx, obj_idx, "get");

    duk_push_c_function(ctx, internalSet, 2);
    if (globalObject)
        duk_put_global_string(ctx, "set");
    else
        duk_put_prop_string(ctx, obj_idx, "set");

    duk_push_c_function(ctx, internalPatchLocal, 1);
    if (globalObject)
        duk_put_global_string(ctx, "patch");
    else
        duk_put_prop_string(ctx, obj_idx, "patch");

    duk_push_c_function(ctx, internalWatch, 1);
    if (globalObject)
        duk_put_global_string(ctx, "watch");
    else
        duk_put_prop_string(ctx, obj_idx, "watch");

    duk_push_c_function(ctx, internalUnwatch, 1);
    if (globalObject)
        duk_put_global_string(ctx, "unwatch");
    else
        duk_put_prop_string(ctx, obj_idx, "unwatch");

    duk_push_c_function(ctx, internalCall, 1);
    if (globalObject)
        duk_put_global_string(ctx, "call");
    else
        duk_put_prop_string(ctx, obj_idx, "call");
}

static duk_ret_t internalLog(duk_context *ctx) {
    int i = 0;
    while (duk_get_type(ctx, i) != DUK_TYPE_NONE) {
        if (duk_is_object(ctx, i) && !duk_is_function(ctx, i))
            cout << duk_json_encode_bytecode(ctx, i);
        else if (duk_is_function(ctx, i))
            cout << duk_function_to_bytecode(ctx);
        else
            cout << duk_to_string(ctx, i);
        i++;
    }
    cout << endl;
    return 0;
}

static duk_ret_t internalStringLoad(duk_context *ctx) {
    if (!duk_is_string(ctx, 0))
        return 0;

    ifstream t(duk_get_string(ctx, 0));
    string str((istreambuf_iterator<char>(t)), istreambuf_iterator<char>());

    duk_push_string(ctx, str.c_str());
    return 1;
}

static duk_ret_t internalStringSave(duk_context *ctx) {
    if (!duk_is_string(ctx, 0) || !duk_is_string(ctx, 1))
        return 0;

    ofstream out(duk_get_string(ctx, 0));
    out << duk_get_string(ctx, 1);
    out.close();

    return 0;
}

static duk_ret_t internalStringReplace(duk_context *ctx) {
    if (!duk_is_string(ctx, 0) || !duk_is_string(ctx, 1) || !duk_is_string(ctx, 2))
        return 0;

    string str = duk_get_string(ctx, 0);
    string from = duk_get_string(ctx, 1);
    string to = duk_get_string(ctx, 2);

    str = stringReplace(str, from, to);

    duk_push_string(ctx, str.c_str());
    return 1;
}

static duk_ret_t internalTotal(duk_context *ctx) {
    duk_push_number(ctx, Atom::atomsTotal());
    return 1;
}

static duk_ret_t internalTimestamp(duk_context *ctx) {
    time_t result = time(nullptr);
    duk_push_int(ctx, result);
    return 1;
}

static duk_ret_t internalShutdown(duk_context *ctx) {
    cout << "Shutting down..." << endl;
    Atom::shutdown = true;
    return 0;
}

static duk_ret_t internalApiNew(duk_context *ctx) {
    if (!duk_is_object(ctx, 0))
        return 0;

    json config = json::parse(duk_json_encode(ctx, 0));

    string proto = "http";
    if (config.find("proto") != config.end())
        proto = config["proto"].get<string>();

    int port = 80;
    if (config.find("port") != config.end())
        port = config["port"].get<int>();

    auto w = new WebApi(proto, port);

    duk_push_string(ctx, uuids::to_string(w->id).c_str());
    return 1;
}

static duk_ret_t internalApiDelete(duk_context *ctx) {
    if (duk_get_type(ctx, 0) != DUK_TYPE_STRING)
        return 0;

    auto u = uuid::from_string(duk_get_string(ctx, 0));
    if (u)
        WebApi::webapiGet(*u);

    return 0;
}

static duk_ret_t internalPatchGlobal(duk_context *ctx) {
    if (!duk_is_object(ctx, 0))
        return 0;

    Atom::patch(duk_json_encode_bytecode(ctx, 0), duk_is_string(ctx, 1) ? duk_get_string(ctx, 1) : "");

    return 0;
}

static duk_ret_t internalQuery(duk_context *ctx) {
    // Selector
    if (!duk_is_string(ctx, 0))
        return 0;
    string selector = duk_get_string(ctx, 0);

    // Atoms
    auto atoms = getAtomsFromThis(ctx);

    auto result = Atom::query(atoms, selector);

    pushQueryObject(ctx, result, false);
    return 1;
}

static duk_ret_t internalPatchLocal(duk_context *ctx) {
    if (!duk_is_object(ctx, 0))
        return 0;

    auto jsonPatch = duk_json_encode_bytecode(ctx, 0);

    // Atoms
    auto atoms = getAtomsFromThis(ctx);

    for (auto &aid : atoms) {
        auto a = Atom::atomGet(aid);
        if (a != nullptr)
            a->patchInternal(jsonPatch);
    }

    // Push query object for fluent interface
    duk_push_this(ctx);
    if (duk_is_undefined(ctx, -1)) {
        duk_pop(ctx);
        pushQueryObject(ctx, atoms, false);
    }
    return 1;
}

static duk_ret_t internalDump(duk_context *ctx) {
    // Depth
    int depth = -1;
    if (duk_is_number(ctx, 0))
        depth = duk_get_int(ctx, 0);

    // Atoms
    auto atoms = getAtomsFromThis(ctx);

    json result = Atom::dump(atoms, depth);

    duk_push_string(ctx, result.dump().c_str());
    duk_json_decode(ctx, -1);
    return 1;
}

static duk_ret_t internalDestroy(duk_context *ctx) {
    auto atoms = getAtomsFromThis(ctx);

    // Atom
    Atom::destroy(atoms);

    return 0;
}

static duk_ret_t internalGet(duk_context *ctx) {
    // Field
    if (!duk_is_string(ctx, 0))
        return 0;
    string field = duk_get_string(ctx, 0);

    // Atoms
    auto atoms = getAtomsFromThis(ctx);

    // Walk through atoms
    json result = json::array();
    for (auto &aid : atoms) {
        auto a = Atom::atomGet(aid);
        if (a != nullptr && a->fieldExists(field))
            result.push_back(json::parse(a->fieldGet(field)));
    }

    duk_push_string(ctx, result.dump().c_str());
    duk_json_decode(ctx, -1);
    return 1;
}

static duk_ret_t internalSet(duk_context *ctx) {
    // Field
    if (!duk_is_string(ctx, 0))
        return 0;
    string field = duk_get_string(ctx, 0);

    // Value
    string value = duk_value_to_json(ctx, 1).dump();

    // Atoms
    auto atoms = getAtomsFromThis(ctx);

    // Walk through atoms
    for (auto &aid : atoms) {
        auto a = Atom::atomGet(aid);
        if (a != nullptr)
            a->fieldSet(field, value);
    }

    // Push query object for fluent interface
    duk_push_this(ctx);
    if (duk_is_undefined(ctx, -1)) {
        duk_pop(ctx);
        pushQueryObject(ctx, atoms, false);
    }
    return 1;
}

static duk_ret_t internalWatch(duk_context *ctx) {
    // Watcher uuid
    if (!duk_is_string(ctx, 0))
        return 0;
    auto watcherUuidOpt = uuid::from_string(duk_get_string(ctx, 0));
    if (!watcherUuidOpt)
        return 0;
    uuid watcherUuid = *watcherUuidOpt;

    // Atoms
    auto atoms = getAtomsFromThis(ctx);

    // Walk through atoms
    for (auto &aid : atoms) {
        auto a = Atom::atomGet(aid);
        if (a != nullptr)
            a->watch(watcherUuid);
    }

    // Push query object for fluent interface
    duk_push_this(ctx);
    if (duk_is_undefined(ctx, -1)) {
        duk_pop(ctx);
        pushQueryObject(ctx, atoms, false);
    }
    return 1;
}

static duk_ret_t internalUnwatch(duk_context *ctx) {
    // Watcher uuid
    if (!duk_is_string(ctx, 0))
        return 0;
    auto watcherUuidOpt = uuid::from_string(duk_get_string(ctx, 0));
    if (!watcherUuidOpt)
        return 0;
    uuid watcherUuid = *watcherUuidOpt;

    // Atoms
    auto atoms = getAtomsFromThis(ctx);

    // Walk through atoms
    for (auto &aid : atoms) {
        auto a = Atom::atomGet(aid);
        if (a != nullptr)
            a->unwatch(watcherUuid);
    }

    // Push query object for fluent interface
    duk_push_this(ctx);
    if (duk_is_undefined(ctx, -1)) {
        duk_pop(ctx);
        pushQueryObject(ctx, atoms, false);
    }
    return 1;
}

static duk_ret_t internalUpdates(duk_context *ctx) {
    // Watcher uuid
    if (!duk_is_string(ctx, 0))
        return 0;
    auto watcherUuidOpt = uuid::from_string(duk_get_string(ctx, 0));
    if (!watcherUuidOpt)
        return 0;
    uuid watcherUuid = *watcherUuidOpt;

    json updatesJson = Watcher::watcherGet(watcherUuid)->extract();

    duk_push_string(ctx, updatesJson.dump().c_str());
    duk_json_decode(ctx, -1);
    return 1;
}

static duk_ret_t internalContextNew(duk_context *ctx) {
    auto c = new Context();
    duk_push_string(ctx, uuids::to_string(c->id).c_str());
    return 1;
}

static duk_ret_t internalContextDelete(duk_context *ctx) {
    auto contextUuidOpt = uuid::from_string(duk_get_string(ctx, 0));
    if (!contextUuidOpt)
        return 0;
    delete Context::contextGet(*contextUuidOpt);
    return 0;
}

static duk_ret_t internalCall(duk_context *ctx) {
    if (!duk_is_object(ctx, 0))
        return 0;

    // Atoms from query object
    auto atoms = getAtomsFromThis(ctx);

    json params = duk_json_encode_bytecode(ctx, 0);

    // Context param
    string contextIdStr;
    if (params.find("context") != params.end()) {
        contextIdStr = params["context"].get<string>();
    } else {
        duk_get_global_string(ctx, "contextId");
        contextIdStr = duk_get_string(ctx, -1);
    }
    auto contextId = uuid::from_string(contextIdStr);
    if (!contextId)
        return 0;
    auto c = Context::contextGet(*contextId);
    if (c == nullptr)
        return 0;

    // Code param
    string code;
    if (params.find("code") != params.end())
        code = params["code"].get<string>();
    else
        return 0;

    // Each param
    bool each = false;
    if (params.find("each") != params.end() && params["each"].type() == json::value_t::boolean)
        each = params["each"].get<bool>();

    json r = json::array();

    if (each)
        for (auto &aid : atoms) {
            auto a = Atom::atomGet(aid);
            if (a != nullptr)
                r.push_back(c->execute(code, {aid}, {}, c->headersInput, c->headersOutput));
        }
    else
        r = c->execute(code, atoms, {}, c->headersInput, c->headersOutput);

    duk_push_string(ctx, r.dump().c_str());
    duk_json_decode(ctx, -1);
    return 1;
}

static duk_ret_t internalFunctionCall(duk_context *ctx) {
    duk_idx_t nargs = duk_get_top(ctx);
    vector<json> args;
    for (int i = 0; i < nargs; i++)
        args.push_back(duk_value_to_json(ctx, i));

    duk_push_current_function(ctx);
    duk_get_prop_string(ctx, -1, "function_name");
    string functionName = duk_safe_to_string(ctx, -1);

    duk_get_global_string(ctx, "contextId");
    string contextId = duk_get_string(ctx, -1);
    auto c = Context::contextGet(*uuid::from_string(contextId));
    if (c == nullptr)
        return 0;

    // Atoms from query object
    auto atoms = getAtomsFromThis(ctx);

    json r = json::array();

    // Walk through atoms
    for (auto &aid : atoms) {
        auto a = Atom::atomGet(aid);
        if (a != nullptr) {
            string functionCode = a->fieldGet(functionName);

            if (functionCode.empty()) {
                json null_value;
                r.push_back(null_value);
            }

            // Execute function
            else if (!functionCode.empty() && functionCode.find("\"bytecode_") == 0 && functionCode[functionCode.size() - 1] == '"')
                r.push_back(c->execute(functionCode.substr(1, functionCode.size() - 2), {aid}, args, c->headersInput, c->headersOutput));

            // Field getter
            else
                r.push_back(json::parse(functionCode));
        }
    }

    duk_push_string(ctx, r.dump().c_str());
    duk_json_decode(ctx, -1);
    return 1;
}

static duk_ret_t internalTimerNew(duk_context *ctx) {
    duk_get_global_string(ctx, "contextId");
    string contextId = duk_get_string(ctx, -1);
    duk_pop(ctx);
    auto c = Context::contextGet(*uuid::from_string(contextId));
    if (c == nullptr)
        return 0;

    if (!duk_is_function(ctx, 0))
        return 0;

    int iterations = 0;
    if (duk_is_number(ctx, 2)) {
        iterations = duk_get_int(ctx, 2);
        duk_pop(ctx);
    }

    int interval = 1000;
    if (duk_is_number(ctx, 1)) {
        interval = duk_get_int(ctx, 1);
        duk_pop(ctx);
    }

    auto code = duk_function_to_bytecode(ctx);

    auto timerId = c->timerNew(code, interval, iterations);

    duk_push_string(ctx, uuids::to_string(timerId).c_str());
    return 1;
}

static duk_ret_t internalTimerDelete(duk_context *ctx) {
    duk_get_global_string(ctx, "contextId");
    string contextId = duk_get_string(ctx, -1);
    duk_pop(ctx);
    auto c = Context::contextGet(*uuid::from_string(contextId));
    if (c == nullptr)
        return 0;

    if (!duk_is_string(ctx, 0))
        return 0;
    auto timerId = uuid::from_string(duk_get_string(ctx, 0));

    if (timerId)
        c->timerDelete(*timerId);

    return 0;
}

static duk_ret_t internalHeaderSet(duk_context *ctx) {
    if (!duk_is_string(ctx, 0) || !duk_is_string(ctx, 1))
        return 0;

    duk_get_global_string(ctx, "contextId");
    string contextId = duk_get_string(ctx, -1);
    duk_pop(ctx);
    auto c = Context::contextGet(*uuid::from_string(contextId));
    if (c == nullptr)
        return 0;

    c->headerSet(duk_get_string(ctx, 0), duk_get_string(ctx, 1));

    return 0;
}

static duk_ret_t internalHeaders(duk_context *ctx) {
    duk_get_global_string(ctx, "contextId");
    string contextId = duk_get_string(ctx, -1);
    duk_pop(ctx);
    auto c = Context::contextGet(*uuid::from_string(contextId));
    if (c == nullptr)
        return 0;

    json headersJson = c->headersGet();

    duk_push_string(ctx, headersJson.dump().c_str());
    duk_json_decode(ctx, -1);
    return 1;
}

Context::Context(uuid newid) {
    ctx = duk_create_heap_default();

    id = newid;

    contextsMutex.lock();
    contexts.insert(pair<uuid, Context*>(id, this));
    contextsMutex.unlock();

    timersThread = new thread([this]() {
        timersThreadProcess();
    });
    timersThread->detach();
}

Context::Context() : Context::Context(uuid_system_generator{}()) {}

Context::~Context() {
    timersThreadActive = false;
    while (!timersThreadFinished)
        this_thread::sleep_for(chrono::milliseconds(10));
    delete timersThread;

    contextsMutex.lock();
    contexts.erase(id);
    contextsMutex.unlock();

    duk_destroy_heap(ctx);
}

void Context::timersThreadProcess() {
    const int interationsInterval = 10;
    while (timersThreadActive) {
        vector<uuid> timersToDelete;
        for (auto const& [timerId, t] : timers) {
            if (t->timeLeft <= 0) {
                execute(t->code);
                t->timeLeft = t->interval;
                t->iterations--;
                if (t->iterations == 0)
                    timersToDelete.push_back(timerId);
            }
            t->timeLeft -= interationsInterval;
        }
        for (auto &t : timersToDelete)
            timerDelete(t);
        this_thread::sleep_for(chrono::milliseconds(interationsInterval));
    }
    timersThreadFinished = true;
}

uuid Context::timerNew(const string& code, int interval, int iterations) {
    auto t = new Timer();
    t->interval = interval;
    t->timeLeft = interval;
    t->code = code;
    t->iterations = iterations;

    uuid timerId = uuid_system_generator{}();
    timers.insert(pair<uuid, Timer*>(timerId, t));

    return timerId;
}

void Context::timerDelete(uuid timerId) {
    delete timers.find(timerId)->second;
    timers.erase(timerId);
}

Context* Context::contextGet(uuid contextId) {
    contextsMutex.lock();
    auto p = contexts.find(contextId);
    if (p == contexts.end()) {
        contextsMutex.unlock();
        return nullptr;
    }
    contextsMutex.unlock();
    return p->second;
}

void Context::headerSet(const string& key, const string& value) {
    if (headersOutput != nullptr)
        headersOutput->insert(pair<string, string>(key, value));
}

json Context::headersGet() {
    if (headersInput == nullptr)
        return json();
    json r;
    for (auto const& [key, val] : *headersInput)
        r.emplace(key, val);
    return r;
}

json Context::execute(const string& code, vector<AtomId> atoms, vector<json> args,
    SimpleWeb::CaseInsensitiveMultimap* newHeadersInput,
    SimpleWeb::CaseInsensitiveMultimap* newHeadersOutput) {

    mutex.lock();

    headersInput = newHeadersInput;
    headersOutput = newHeadersOutput;

    pushQueryObject(ctx, atoms, true);

    // string stringLoad(string filename)
    duk_push_c_function(ctx, internalStringLoad, 1);
    duk_put_global_string(ctx, "stringLoad");

    // void stringSave(string filename, string source)
    duk_push_c_function(ctx, internalStringSave, 2);
    duk_put_global_string(ctx, "stringSave");

    // void stringReplace(string str, string from, string to)
    duk_push_c_function(ctx, internalStringReplace, 3);
    duk_put_global_string(ctx, "stringReplace");

    // void log(...)
    duk_push_c_function(ctx, internalLog, DUK_VARARGS);
    duk_put_global_string(ctx, "log");

    // int total()
    duk_push_c_function(ctx, internalTotal, 0);
    duk_put_global_string(ctx, "total");

    // int timestamp()
    duk_push_c_function(ctx, internalTimestamp, 0);
    duk_put_global_string(ctx, "timestamp");

    // void shutdown()
    duk_push_c_function(ctx, internalShutdown, 0);
    duk_put_global_string(ctx, "shutdown");

    // uuid apiNew({proto: "http", port: 80})
    duk_push_c_function(ctx, internalApiNew, 1);
    duk_put_global_string(ctx, "apiNew");

    // void apiDelete(uuid apiUuid)
    duk_push_c_function(ctx, internalApiDelete, 1);
    duk_put_global_string(ctx, "apiDelete");

    // void patchGlobal(json patch, string cluster = "")
    // void patch(json patch, string cluster = "")
    duk_push_c_function(ctx, internalPatchGlobal, DUK_VARARGS);
    duk_put_global_string(ctx, "patchGlobal");
    duk_push_c_function(ctx, internalPatchGlobal, DUK_VARARGS);
    duk_put_global_string(ctx, "patch");

    // vector<AtomId> query(string selector)
    duk_push_c_function(ctx, internalQuery, 1);
    duk_put_global_string(ctx, "query");

    // json updates(uuid watcherUuid)
    duk_push_c_function(ctx, internalUpdates, 1);
    duk_put_global_string(ctx, "updates");

    // uuid contextNew()
    duk_push_c_function(ctx, internalContextNew, 0);
    duk_put_global_string(ctx, "contextNew");

    // void contextDelete(uuid existingContextUuid)
    duk_push_c_function(ctx, internalContextDelete, 1);
    duk_put_global_string(ctx, "contextDelete");

    // json call({context: contextId, code: function/bytecode, each: false})
    duk_push_c_function(ctx, internalCall, 1);
    duk_put_global_string(ctx, "call");

    // uuid timerNew(string code, int interval = 1000, int iterations = 0)
    duk_push_c_function(ctx, internalTimerNew, DUK_VARARGS);
    duk_put_global_string(ctx, "timerNew");

    // void timerDelete(uuid timerId)
    duk_push_c_function(ctx, internalTimerDelete, 1);
    duk_put_global_string(ctx, "timerDelete");

    // contextId
    duk_push_string(ctx, uuids::to_string(id).c_str());
    duk_put_global_string(ctx, "contextId");

    // json headersGet()
    duk_push_c_function(ctx, internalHeaders, 1);
    duk_put_global_string(ctx, "headers");

    // string headerSet(string key)
    duk_push_c_function(ctx, internalHeaderSet, 2);
    duk_put_global_string(ctx, "headerSet");

    // Execute the code
    duk_int_t error;
    if (code.find("bytecode_") == 0) {
        string decodedBytecode = base64_decode(code.substr(9));
        duk_push_external_buffer(ctx);
        duk_config_buffer(ctx, -1, &decodedBytecode[0], decodedBytecode.size());
        duk_load_function(ctx);
        for (auto &j : args)
            duk_push_value_from_json(ctx, j);
        error = duk_pcall(ctx, args.size());
    }
    else
        error = duk_peval_string(ctx, code.c_str());

    if (error != 0) {
        cout << "Execution error" << endl;
        json null_value;
        mutex.unlock();
        return null_value;
    }

    auto result = duk_value_to_json(ctx, -1);

    mutex.unlock();

    return result;
}