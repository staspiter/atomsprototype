apiNew({ proto: 'http', port: 80 });

patch({
    $: "cluster",
    $children: [
        {
            $: "test",
            "foo": "bar"
        },
        {
            "foo": "bar"
        },
        {
            "foo": "bar"
        }
    ],
    f: function(a, b) {
        headerSet("Content-Type", "text/html; charset=utf-8");
        return a + b;
    }
});

/*

var timerCounter = 1;
var testTimer = timerNew(function(){
    log(timerCounter);
    timerCounter++;
}, 100, 50);

timerNew(function(){
    log("Stop timer");
    timerDelete(testTimer);
}, 2500, 1);

 */

/*

call({code: function() {
    log(this);
}});

log(query("cluster").f(10, 5)[0]);

log(query("cluster").dump(-1));

log(query("cluster.children[0]"));

log(query("cluster.children[0]").dump(0));

log(query("cluster.children[0]").get("foo"));

query("cluster.children[0]").set("foo", "123");

log(query("cluster.children[0]").get("foo"));

query("cluster").query(".children").destroy();

log(query("cluster").dump(-1));

 */