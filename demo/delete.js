patch({
    $: "cluster",
    $children: [
        {
            "foo": "bar"
        },
        {
            "foo": "bar"
        },
        {
            "foo": "bar"
        }
    ]
});

log(total());

destroy(query("$cluster.children"));

log(total());

shutdown();