#include "WebApi.h"

#include "Atom.h"
#include "Context.h"

unordered_map<uuid, WebApi*> WebApi::webapis;
recursive_mutex WebApi::webapisMutex;

WebApi::WebApi(string proto, int port) {
    this->proto = proto;
    this->port = port;

    if (proto == "http") {
        server.config.port = port;

        server.resource["^(.*?)$"]["GET"] = [this](shared_ptr<HttpServer::Response> response, shared_ptr<HttpServer::Request> request) {
            stringstream stream;

            string fullPath = request->path;
            if (!request->query_string.empty())
                fullPath += "?" + request->query_string;
            auto p = request->header.find("host");
            if (p != request->header.end())
                fullPath = p->second + fullPath;

            string selector, command;

            auto querstionMarkPos = fullPath.find('?');

            selector = SimpleWeb::Percent::decode(fullPath.substr(0, querstionMarkPos));
            if (selector[selector.length() - 1] == '/')
                selector = selector.substr(0, selector.length() - 1);

            if (querstionMarkPos != string::npos)
                command = SimpleWeb::Percent::decode(fullPath.substr(querstionMarkPos + 1));

            auto atoms = Atom::query({}, selector);

            Context ctx;

            SimpleWeb::CaseInsensitiveMultimap inputHeaders = request->header;
            SimpleWeb::CaseInsensitiveMultimap outputHeaders;

            json result;
            if (!command.empty())
                result = ctx.execute(command, atoms, {}, &inputHeaders, &outputHeaders);

            string resultStr;
            if (result.type() == json::value_t::string)
                resultStr = result.get<string>();
            else
                resultStr = result.dump();

            stream << resultStr;

            response->write(stream, outputHeaders);
        };

        serverThread = new thread([this]() {
            server.start();
        });
        serverThread->detach();
    }

    id = uuid_system_generator{}();
    webapisMutex.lock();
    webapis.insert(pair<uuid, WebApi*>(id, this));
    webapisMutex.unlock();
}

WebApi::~WebApi() {
    webapisMutex.lock();
    webapis.erase(id);
    webapisMutex.unlock();

    delete serverThread;
}

WebApi* WebApi::webapiGet(uuid webapiId) {
    webapisMutex.lock();
    auto p = webapis.find(webapiId);
    if (p == webapis.end()) {
        webapisMutex.unlock();
        return nullptr;
    }
    webapisMutex.unlock();
    return p->second;
}
