#ifndef ATOMS_UTILS_H
#define ATOMS_UTILS_H

#include <string>
#include <vector>

using namespace std;

vector<string> strSplit(const string& str, string sep);
vector<string> strSplitWithQuotes(const string &s, char sep, char quotesOpen, char quotesClose);
std::string stringReplace(std::string subject, const std::string& search, const std::string& replace);

void ltrim(std::string &s);
void rtrim(std::string &s);
void trim(std::string &s);
bool is_number(const std::string& s);

bool ends_with(std::string const & value, std::string const & ending);

#endif //ATOMS_UTILS_H
