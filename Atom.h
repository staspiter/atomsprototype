#ifndef ATOMS_ATOM_H
#define ATOMS_ATOM_H

#include <map>
#include <set>
#include <string>
#include <iostream>
#include <algorithm>
#include <exception>
#include <regex>
#include <utility>

#include "json.hpp"
#include "uuid.h"

#include "AtomId.h"
#include "WebApi.h"
#include "Watcher.h"

using namespace std;
using namespace nlohmann;
using namespace uuids;

class AtomIdAlreadyExistsException: public exception {
    virtual const char* what() const throw() {
        return "Specified atom id already exists";
    }
};

class Atom {
public:
    explicit Atom(const AtomId& specificId);
    explicit Atom(string cluster);
    ~Atom();

    AtomId id;

    static unsigned long long int atomsTotal();
    static Atom* atomGet(AtomId id);

    static void patch(json patchObj, string cluster);
    void patchInternal(json patchObj);

    void dfs(vector<AtomId> &visited, int depth = -1);

    static json dump(vector<AtomId> atoms, int depth = -1);
    static vector<AtomId> query(vector<AtomId> atoms, string selector);
    static void destroy(const vector<AtomId>& atoms);

    void fieldSet(string key, string value);
    void fieldSet(string key, json value);
    bool fieldExists(const string& key);
    string fieldGet(const string& key);
    vector<pair<string,string>> fieldsAllGet();

    void linkAdd(const string& key, AtomId id);
    void linkDelete(const string& key, AtomId id);
    vector<AtomId> linksGet(string key, bool external = false);
    void linksDisconnectAll();
    unsigned long long int linksCount();

    void watch(uuid watcherId);
    void unwatch(uuid watcherId);

    void lock();
    void unlock();

    static bool shutdown;

private:
    static unordered_map<AtomId, Atom*> atoms;
    static recursive_mutex atomsMutex;

    void watchersAddPatch(const json& patch);

    recursive_mutex mutex;

    // Private access to these fields should be in a locked clause
    unordered_map<string, string> fields;
    multimap<string, AtomId> links;
    multimap<string, AtomId> linksExternal;
    set<uuid> watchers;
};

string readSelector(string s, string &rest);

#endif //ATOMS_ATOM_H
