#include "Watcher.h"

unordered_map<uuid, Watcher*> Watcher::watchers;
recursive_mutex Watcher::watchersMutex;

Watcher::Watcher(uuid id) {
    this->id = id;

    watchersMutex.lock();
    watchers.insert(pair<uuid, Watcher*>(id, this));
    watchersMutex.unlock();
}

Watcher::~Watcher() {
    watchersMutex.lock();
    watchers.erase(id);
    watchersMutex.unlock();
}

void Watcher::store(AtomId& aid, json patch) {
    lock();
    if (storedPatch.find(aid) == storedPatch.end()) {}
    storedPatch.insert(pair<AtomId, json>(aid, json()));
    patch["$"] = aid.toString();
    storedPatch.find(aid)->second.update(patch);
    unlock();
}

json Watcher::extract() {
    json r = json::array();
    lock();
    for (auto& [key, value]: storedPatch)
        r.push_back(value);
    storedPatch.clear();
    unlock();
    return r;
}

Watcher* Watcher::watcherGet(uuid watcherId) {
    Watcher* r;
    watchersMutex.lock();
    if (watchers.find(watcherId) == watchers.end())
        new Watcher(watcherId);
    r = watchers.find(watcherId)->second;
    watchersMutex.unlock();
    return r;
}

void Watcher::lock() {
    mutex.lock();
}

void Watcher::unlock() {
    mutex.unlock();
}