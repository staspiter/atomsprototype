#ifndef ATOMS_ATOMID_H
#define ATOMS_ATOMID_H

#include <string>

#include "json.hpp"
#include "uuid.h"

using namespace std;
using namespace nlohmann;
using namespace uuids;

class AtomId {
public:
    AtomId();
    explicit AtomId(string atomIdStr);
    static AtomId AtomIdGenerate(string newCluster);
    static AtomId AtomIdEmpty();
    string toString();

    string cluster;
    uuid id;
    bool valid;
    static uuid clusterRootUuid;

    bool isClusterRoot();

    // Converts json array to AtomId vector
    static vector<AtomId> jsonToAtomIdsVector(json j);

    // Extracts atom id from atom json
    static AtomId extractIdFromJson(json j);

    bool operator==(const AtomId &rhs) const;
    bool operator!=(const AtomId &rhs) const;
};

namespace std {
    template <>
    struct hash<AtomId> {
        size_t operator()(const AtomId& aid) const {
            return hash<string>()(aid.cluster) ^ (hash<uuid>()(aid.id) << 1);
        }
    };
}

#endif //ATOMS_ATOMID_H
